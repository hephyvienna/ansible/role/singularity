import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_singularity_package(host):
    p = host.package('singularity')

    assert p.is_installed


def test_singularity_conf(host):
    f = host.file('/etc/singularity/singularity.conf')

    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'
