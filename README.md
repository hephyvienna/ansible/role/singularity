# singularity [![Build Status](https://travis-ci.com/hephyvienna/ansible-role-singularity.svg?branch=master)](https://travis-ci.com/hephyvienna/ansible-role-singularity) [![Ansible Role](https://img.shields.io/ansible/role/40971.svg)](https://galaxy.ansible.com/hephyvienna/singularity)


Install Singularity from [SyLabs](https://www.sylabs.io)

## Requirements

*   EL6/7
*   EPEL

## Role Variables

Singularity installation

    singularity_pkgs:
      - singularity

Singularity configuration. [Details](https://www.sylabs.io/guides/2.6/admin-guide/)

    singularity_setuid: yes
    singularity_max_loop_devices: 256
    singularity_allow_pid_ns: yes
    singularity_config_passwd: yes
    singularity_config_group: yes
    singularity_config_resolv_conf: yes
    singularity_mount_proc: yes
    singularity_mount_sys: yes
    singularity_mount_dev: yes
    singularity_mount_devpts: yes
    singularity_mount_home: yes
    singularity_mount_tmp: yes
    singularity_mount_hostfs: no
    singularity_bind_paths:
      - /etc/localtime
      - /etc/hosts
    singularity_bind_control: yes
    singularity_enable_overlay: try
    singularity_enable_underlay: no
    singularity_mount_slave: yes
    singularity_sessiondir_maxsize: 16
    singularity_limit_container_owners: []
    singularity_limit_container_groups: []
    singularity_limit_container_paths: []
    singularity_allow_containers:
      squashfs: yes
      extfs: yes
      dir: yes
    singularity_autofs_bug_paths: []
    singularity_memory_fs_type: tmpfs
    singularity_always_use_nv: no


## Example Playbook

    - hosts: servers
      roles:
         - hephyvienna.singularity

## License

MIT

## Author Information

Written by [Dietrich Liko](http://hephy.at/dliko) in April 2019

[Institute for High Energy Physics](http://www.hephy.at) of the
[Austrian Academy of Sciences](http://www.oeaw.ac.at)
